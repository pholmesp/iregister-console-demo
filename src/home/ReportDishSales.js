import React, { Component } from 'react'
import { Table } from 'react-bootstrap'
import {API} from "../config";


export default class AllTodayFinishedOrdersTab extends Component{
    constructor(props){
        super(props);
        this.state={
            //DateRange:'Last 7 Days',
            DishReport:[]
        }
    }
    componentDidMount(){
        this.getData();
    }
    getData(){
        fetch(API.baseUri+API.Report+'/'+this.props.url)
            .then((response) => {
                if (response.status === 200) {
                    return response.json()
                } else console.log("Get data error ");
            }).then((json) =>{
            // console.log(json)
            this.setState({
                //DateRange: json.date,
                DishReport: json.stat
            })
        }).catch((error) => {
            console.log('error on .catch', error);
        })
    }
    render(){
        return(
            <div className="col-lg-12 nova-card cust-border">
                <center><b3><b>菜品销量报告 </b></b3></center>
                <Table striped bordered condensed hover>
                    <thead>
                    <tr>
                        <th className="th-width-DishID">菜品名称</th>
                        <th className="th-width-DishID">菜品销量</th>

                    </tr>
                    </thead>
                    <tbody>
                    {this.state.DishReport.map((Report,a)=>{
                        return(
                            <tr key={a}>
                                <td>
                                    {Report.name}
                                </td>
                                <td>
                                    {Report.total}
                                </td>
                            </tr>
                        )
                    })}
                    </tbody>
                </Table>
            </div>
        )
    }
}