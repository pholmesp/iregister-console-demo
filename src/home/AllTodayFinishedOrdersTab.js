import React, { Component } from 'react'
import { Table,Button } from 'react-bootstrap'


export default class AllTodayFinishedOrdersTab extends Component{
    render(){
        return(
              <Table striped bordered condensed hover>
                <thead>
                <tr>
                    <th className="th-width-DishID">订单编号</th>
                    <th className="th-width-DishID">菜品总览</th>
                    <th className="th-width-DishName">生成时间</th>
                    <th className="th-width-DishPrice">订单总价</th>
                    <th className="th-width-DishType">订单类别</th>
                    <th className="th-width-DishPrint">订单打印</th>

                </tr>
                </thead>
                  <tbody>
                {this.props.DistinctID.map((array, a) =>{
                    return (
                        <tr key={a}>
                        {this.props.OrderType === 0 || array.OrderType === this.props.OrderType?

                                    <td>
                                        {array.id}
                                    </td>
                            :null}

                            {this.props.OrderType === 0 || array.OrderType === this.props.OrderType?
                                     <td >
                                    {this.props.FindName(array.id).map((dish,i) =>

                                            <div key={i}>
                                                {dish.deleted === 0 ?
                                                    <div>{dish.name} 数量: [{dish.DishCount}]</div>
                                                    :
                                                    <div className="strikeThrough">{dish.name} 数量:
                                                        [{dish.DishCount}]</div>
                                                }
                                            </div>
                                    )}
                                    </td>
                                :null}

                            {this.props.OrderType === 0 || array.OrderType === this.props.OrderType?
                                    <td>
                                        {array.creatTime.slice(array.creatTime.lastIndexOf('T') + 0).replace("T", "").replace("+1000", "").replace("+0000", "").replace("+1100", "")} {array.creatTime.slice(0, array.creatTime.indexOf('T'))}
                                    </td>
                                :null}

                            {this.props.OrderType === 0 || array.OrderType === this.props.OrderType?
                                    <td>
                                        {array.finalPrice}
                                    </td>
                                        :null}
                            {this.props.OrderType === 0 || array.OrderType === this.props.OrderType?
                                    <td>
                                        {array.OrderType === 1 ?
                                            <div>
                                                堂吃
                                            </div>
                                            :
                                            <div>
                                                外卖
                                            </div>}
                                    </td>
                                :null}
                            {this.props.OrderType === 0 || array.OrderType === this.props.OrderType?
                                <td>
                                    <Button bsSize="large" bsStyle="success" onClick={()=>{this.props.PrintReceipt(array)}}>再次打印小票</Button>
                                </td>
                                :
                                null
                            }
                                

                        </tr>
                    )
                })}
                  </tbody>
            </Table>
        )
    }
}